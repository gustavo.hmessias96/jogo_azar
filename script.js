    const numImagens = 7;
    const namePrimeiroImg = 1;
    let numLeft = 0;
    let numCenter = 0;
    let numRight = 0;
    let pontosIniciais =  10;
    let pontosFinais =  0
    const acerta2 = 3;
    const acerta3 = 10;
    const diretorio  = "./images/" ;
    const formato = ".png";
    const imgLeft = document.getElementById("left");
    const imgCenter = document.getElementById("center");
    const imgRight = document.getElementById("right");
    const estrela = document.createElement('span')
    const button = document.getElementById("jogar");
    const buttonAgain =  document.getElementById("play");
    let finalTurno = true
    let enableButton = true;
    pontosFinais = pontosIniciais
   

    function numAleatorio() {
       
        return namePrimeiroImg + Math.floor((numImagens) * Math.random());
    }
    
    function trocarImgleft() {
        
        numLeft = numAleatorio()
        
        imgLeft.src =`${diretorio}${numLeft}${formato}`;
        
    }
    
    function trocarImgCenter() {
        
        numCenter = numAleatorio()
        
        imgCenter.src =`${diretorio}${numCenter}${formato}`;
    }

    function trocarImgRight() {
        
        numRight = numAleatorio()
        
        imgRight.src =`${diretorio}${numRight}${formato}`;
    }
    
    function fundoAmareloImg() {
      if(numLeft === numRight){
        imgLeft.className ="imgSelecionada"
        imgRight.className ="imgSelecionada"
      }

      if(numCenter=== numRight){
        imgCenter.className ="imgSelecionada"
        imgRight.className ="imgSelecionada"
      }


      if(numCenter === numLeft){
        imgCenter.className ="imgSelecionada"
        imgLeft.className = "imgSelecionada"
      }

      
      
    }
    function limparFundo(){
      imgCenter.className =""
      imgLeft.className = ""
      imgRight.className =""
    }

    function ganharponto() {
      if(numLeft === numCenter && numLeft === numRight  ){
        pontosFinais+= acerta3
      }

      if(numLeft === numCenter||numLeft === numRight||numRight === numCenter){
        pontosFinais+= acerta2
      }
      
    }

    function gameOver(){
      if(pontosFinais <= 0){
        button.className = "press"
        enableButton =  false
        imgLeft.src =`${diretorio}game-over${formato}`;
        imgCenter.src =`${diretorio}game-over${formato}`;
        imgRight.src =`${diretorio}game-over${formato}`;
      }
    }

    function youWin() {
      if(pontosFinais >= 30){
        imgCenter.className ="imgSelecionada"
        imgLeft.className = "imgSelecionada"
        imgRight.className = "imgSelecionada"
        button.className = "press"
        enableButton =  false
        imgLeft.src =`${diretorio}win${formato}`;
        imgCenter.src =`${diretorio}win${formato}`;
        imgRight.src =`${diretorio}win${formato}`;
      }
      
    }
    
    
    function slot(){
      let r = 0;
      let l = 0;
      let c = 0
      enableButton = false;
      button.className = "press"
             
        const timeLeft =  setInterval(function () {
          l++;
          limparFundo()
          finalTurno = false

          trocarImgleft();
          if(l >= 15){
            clearInterval(timeLeft);
        }
        },100)


      
      const timeCenter =  setInterval(function () {
          c++;
          trocarImgCenter();
          if(c >= 30){
            clearInterval(timeCenter);
        }
      },100)


      
      const timeRight =  setInterval(function () {
          r++;
          trocarImgRight();
          if(r >= 45){

            fundoAmareloImg()
            ganharponto()
            estrela.textContent =  pontosFinais
            button.className =  ""            
            enableButton = true;
            youWin()
            gameOver()
            finalTurno =  true
            clearInterval(timeRight);
            
        }
      },100)

      
      

    }
    function escreverNumEstrelas() {

      estrela.textContent =  pontosFinais
      containerEstrela = document.getElementById("containerEstrela")
      containerEstrela.appendChild(estrela)
      }

      
      button.onclick = function () {
      
        if(enableButton){

        pontosFinais--
        estrela.textContent =  pontosFinais
        slot();
      
        }
       
      
      
      
      }
      buttonAgain.onclick = function () {
        if(finalTurno){
        imgRight.src =`${diretorio}1${formato}`;
        imgCenter.src =`${diretorio}1${formato}`;
        imgLeft.src =`${diretorio}1${formato}`;
        limparFundo()
        pontosFinais = pontosIniciais
        estrela.textContent =  pontosFinais
        button.className =  ""   
        enableButton = true;
        }
      }

escreverNumEstrelas() 





      
      
      